# RadTool

#### 介绍
代替AspNetZeroRadTool

#### 使用说明

1. 将RadToolConsole中的文件复制到AspNetZeroRadTool目录中
2. 将AspNetZeroRadToolVisualStudioExtension.dll和AspNetZeroRadToolVisualStudioExtension.pdb替换到官方生成器安装目录(C:\Users\ThinkPad\AppData\Local\Microsoft\VisualStudio\15.0_a88f7683\Extensions\xxxx)中
3. 修改AspNetZeroRadTool目录中config.json的ProjectType为 Mvc 或者 Angular
4. Visual Studio打开项目 工具>Asp.Net Zero 开始使用



